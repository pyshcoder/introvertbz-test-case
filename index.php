<?php

require __DIR__ . '/vendor/autoload.php';

try {
    $amo = new \AmoCRM\Client('DOMAIN', 'LOGIN', 'APIKEY'); // Подключение к api

    $leadsWithoutTasks = $amo->lead->apiList([
        'filter' => [
            'tasks' => 1,
        ],
    ]); // Выбрать все сделки без задач

    if(count($leadsWithoutTasks) == 0) {
        die('Сделок без задач не найдено.');
    }

    print 'Найдено ' . count($leadsWithoutTasks) . ' сделок без открытых задач <br>';

    $leadsIDs = array_map(function($lead) {
        return $lead['id'];
    }, $leadsWithoutTasks); // Собрать массив id сделок

    $taskModel = $amo->task;
    $taskModel['element_type'] = 2;
    $taskModel['task_type'] = 1;
    $taskModel['text'] = 'Сделка без задачи';

    $tasks = [];

    for($i = 0; $i < count($leadsIDs); $i++) {
        $task = clone $taskModel;
        $task['element_id'] = $leadsIDs[$i];
        $tasks[] = $task;
    }

    print count($tasks) . ' задач будет добавлено <br>';

    $result = $amo->task->apiAdd($tasks); // Добавить все задачи батчем

    if(count($tasks) === count($result)) {
        print 'Успех!';
    } else {
        print 'Ошибка!';
    }

} catch (\AmoCRM\Exception $e) {
    printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
}